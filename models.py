from datetime import date
from trytond.model import ModelSingleton,ModelSQL, ModelView, fields, Workflow
#from trytond.transaction import Transaction
#from trytond.modules.party import Address
from trytond.pyson import Eval
from trytond.pool import Pool
from trytond.report import Report
#from trytond.wizard import Wizard, StateView, StateTransition, StateAction, \
#	StateReport, Button

__all__ = ['Programa','Proyecto','FormatoDeEjecucion','Ingreso','Gasto',
		'CategoriaGasto','Configuracion','GastoReport']

SEPARATOR = ' '
STATES=[
		('borrador','Borrador'),
		('finalizado','Finallizado'),
		('aprobado','Aprobado')
		]
_STATES = {
    'readonly': Eval('state') != 'borrador',
}
_DEPENDS = ['state']

class Programa(ModelSQL,ModelView):
	"Programa"
	#"Identificar unico del modelo, es necesario para referencias posteriores
	__name__= "formatos_de_ejecucion.programas"
	
	#es necesario cuando no existe atribute *name*
	_rec_name='nombre'
	
	#definicion de atributos del modelo
	nombre = fields.Char("Nombre",required=True)
	codigo = fields.Char('Codigo',required=True,select=True,readonly=True,help="Codigo generado por secuencia automatica, ver configuraciones.")
	descripcion = fields.Text("Descripcion")
	activo = fields.Boolean("Activo")
	proyectos = fields.One2Many('formatos_de_ejecucion.proyectos','programa','Proyectos',filter=[('activo','=',True)])
	
	#modificamos la busqueda de registro
	#:clause: es una clausula con tupla(name,condition,value),
	#Reconstruimos la consulta para poder encontrar valores por *nombre* o *descripcion*
	@classmethod
	def search_rec_name (cls,name,clause):
		return['OR',
			   ('nombre',) + tuple(clause[1:]),
			   ('descripcion',) + tuple(clause[1:])
			  ]
	@staticmethod
	def default_activo():
		return True
	
	@classmethod
	def _new_codigo(cls):
		pool = Pool()
		Sequence = pool.get('ir.sequence')
		Configuration = pool.get('formatos_de_ejecucion.configuraciones')
		secuencia_programas = Configuration.read('1',['secuencia_programas'])[0]['secuencia_programas']
		if secuencia_programas:
			return Sequence.get_id(int(secuencia_programas))

	@classmethod
	def create(cls, vlist):
		vlist = [x.copy() for x in vlist]
		for values in vlist:
			if not values.get('codigo'):
				values['codigo'] = cls._new_codigo()
		return super(Programa, cls).create(vlist)
	
class Proyecto(ModelSQL,ModelView):
	"Proyecto"
	__name__ = "formatos_de_ejecucion.proyectos"
	
	#es necesario cuando no existe atribute *name*
	_rec_name='nombre'
	
	#definicion de atributos del modelo
	nombre = fields.Char("Nombre",required=True)
	programa = fields.Many2One('formatos_de_ejecucion.programas','Programa',required=True, domain=[('activo','=',True)])
	codigo = fields.Char('Codigo',required=True,select=True,readonly=True,help="Codigo generado por secuencia automatica, ver configuraciones.")
	descripcion = fields.Text("Descripcion")
	activo = fields.Boolean("Activo")
	formatos_de_ejecucion=fields.One2Many('formatos_de_ejecucion.formatos_de_ejecucion','proyecto','Formatos de Ejecucion',filter=[('fecha_de_ejecucion','>=', date.today().replace(month=1).replace(day=1) )])
	#modificamos la busqueda de registro
	#:clause: es una clausula con tupla(name,condition,value),
	#Reconstruimos la consulta para poder encontrar valores por *title* o *description*
	@classmethod
	def search_rec_name (cls,name,clause):
		return['OR',
			   ('nombre',) + tuple(clause[1:]),
			   ('descripcion',) + tuple(clause[1:])
			  ]	
	
	@staticmethod
	def default_activo():
		return True
	
	@classmethod
	def _new_codigo(cls):
		pool = Pool()
		Sequence = pool.get('ir.sequence')
		Configuration = pool.get('formatos_de_ejecucion.configuraciones')
		secuencia_proyectos = Configuration.read('1',['secuencia_proyectos'])[0]['secuencia_proyectos']
		if secuencia_proyectos:
			return Sequence.get_id(int(secuencia_proyectos))

	@classmethod
	def create(cls, vlist):
		vlist = [x.copy() for x in vlist]
		for values in vlist:
			if not values.get('codigo'):
				values['codigo'] = cls._new_codigo()
		return super(Proyecto, cls).create(vlist)
	
class FormatoDeEjecucion(Workflow, ModelSQL, ModelView):
	"FormatoDeEjecucion"
	__name__ = "formatos_de_ejecucion.formatos_de_ejecucion"
	#cambiar por una secuencia
	#_rec_name = 'fecha_de_ejecucion'
	
	codigo = fields.Char('Codigo',
						required=True,
						select=True,
						readonly=True,
						states=_STATES,
						depends=_DEPENDS,
						help="Codigo generado por secuencia automatica, ver configuraciones."
						)
	fecha_de_ejecucion = fields.Date("Fecha De Ejecucion",
									required=True,
									states=_STATES,
									depends=_DEPENDS,
									)
	factura_de_cobro = fields.Char("Factura de Cobro")#se deberia modificar por una relacion a una tabla de facturas o al modulo de contabilidad donde esta la factura.
	proyecto = fields.Many2One('formatos_de_ejecucion.proyectos',
							'Proyecto',
							required=True,
							domain=[('activo',"=",True)],
							states=_STATES,
							depends=_DEPENDS,
							)
	programa = fields.Function(fields.Char("Programa"),
							"get_nombre_programa"
							)
	lugar = fields.Many2One('party.address',
						"Lugar",
						required=True,
						states=_STATES,
						depends=_DEPENDS,
						domain=[('party','=',Eval('cliente',-1)),('active',"=",True )]
						)
	cliente = fields.Many2One("party.party",
							"Cliente",
							required=True,
							states=_STATES,
							depends=_DEPENDS,
							domain=[('active','=',True),('customer','=',True)]
							)
	observaciones = fields.Text("Observaciones",
							states=_STATES,
							depends=_DEPENDS,
						)
	coordinador = fields.Many2One("party.party",
								"Coordinador/a",
								required=True,
								states=_STATES,
								depends=_DEPENDS,
								domain=[('active','=',True),('coordinator','=',True)]
								)
	gastos = fields.One2Many('formatos_de_ejecucion.gastos',
							"formato_de_ejecucion",
							"Gastos",
							states=_STATES,
							depends=_DEPENDS,
							)
	ingresos = fields.One2Many('formatos_de_ejecucion.ingresos',
							"formato_de_ejecucion",
							"Ingresos",
							states=_STATES,
							depends=_DEPENDS,
							)
	total_gastos = fields.Function(fields.Integer("Total Gastos",readonly = True),"on_change_with_total_gastos")
	total_ingresos = fields.Function(fields.Integer("Total Ingresos",readonly = True),"on_change_with_total_ingresos")
	utilidad = fields.Function(fields.Integer("Utilidad",readonly = True),"on_change_with_utilidad")
	porcentaje_utilidad = fields.Function(fields.Float("Utilidad (%)",(2,2),readonly = True),"on_change_with_porcentaje_utilidad")
	state = fields.Selection(STATES,'State',readonly=True,)
	
	@staticmethod
	def default_state():
		return('borrador')
	
	def get_rec_name(self,fecha_de_ejecucion=None):
		return self.proyecto.nombre + SEPARATOR + self.fecha_de_ejecucion.strftime('%m/%d/%Y')
		
	def get_nombre_programa(self,programa):
		return self.proyecto.programa.nombre
	
	def totalizar_gastos(self):
		total_gastos = 0
		for gasto in self.gastos:
			if not gasto.valor is None:
				total_gastos += gasto.valor
		return total_gastos
	
	@fields.depends("gastos")
	def on_change_with_total_gastos(self,name=None):
		return self.totalizar_gastos()	
	
	def totalizar_ingresos(self):
		total_ingresos = 0
		for ingreso in self.ingresos:
			if not ingreso.valor is None:
				total_ingresos += ingreso.valor
		return total_ingresos

	@fields.depends("ingresos")
	def on_change_with_total_ingresos(self,name=None):
		return self.totalizar_ingresos()
	
	def calcular_utilidad(self):
		utilidad = 0
		try:
			utilidad =  self.totalizar_ingresos() - self.totalizar_gastos()
		except:
			utilidad = 0
		return utilidad
	
	@fields.depends("ingresos","gastos","total_gastos","total_ingresos")
	def on_change_with_utilidad(self,name=None):
		return self.calcular_utilidad()
	
	@fields.depends("ingresos","gastos","total_gastos","total_ingresos")
	def on_change_with_porcentaje_utilidad(self,name=None):
		porcentaje_utilidad = 0.0
		try:
			porcentaje_utilidad =  (float (self.calcular_utilidad()) / self.totalizar_ingresos()) * 100
		except:
			porcentaje_utilidad = 0
		return porcentaje_utilidad
		
	def __getitem__(self,name_field):
		try:
			return getattr(self,name_field)
		except:
			return ""
	def boolean2str(self,valor):
		if valor==1:
			return "SI"
		else:
			return "NO"
	
	def agrupar_gastos(self):
		agrupacion={}
		#categorias={"nombre":[]}
		for gasto in self.gastos:
			if gasto.categoria.nombre in agrupacion:
				agrupacion[gasto.categoria.nombre].append(gasto)
			else:
				agrupacion[gasto.categoria.nombre]=[gasto]
		return agrupacion
	
	@classmethod
	def __setup__(cls):
		super(FormatoDeEjecucion, cls).__setup__()
		cls._error_messages.update({
                'titulo': ('Mensaje "%s" Mensaje\nMensaje.'),
                })
		cls._transitions |= set((
                ('borrador', 'finalizado'),
                ('finalizado', 'aprobado'),
                ('aprobado', 'finalizado'),
                ('finalizado', 'borrador'),
                ))
		cls._buttons.update({
                'finalizar': {
                    'invisible': ~Eval('state').in_(['borrador']),
                    'help': 'Finalizar el formato de ejecucion',
                    'icon': 'tryton-go-next'
                    },
                'aprobar': {
                    'invisible': ~Eval('state').in_(['finalizado']),
                    'help': 'Aprobar el Formato de Ejecucion',
                    'icon': 'tryton-go-next',
                    },
                'desaprobar': {
                    'invisible': ~Eval('state').in_(['aprobado']),
                    'icon': 'tryton-go-previous',
                    },
				'aborrador': {
                    'invisible': ~Eval('state').in_(['finalizado']),
                    'icon': 'tryton-go-previous',
                    },
                })
	
	@classmethod
	def search_rec_name (cls,name,clause):
		return['OR',
			   ('fecha_de_ejecucion',) + tuple(clause[1:]),
			   ('observaciones',) + tuple(clause[1:])
			  ]
	@classmethod
	def _new_codigo(cls):
		pool = Pool()
		Sequence = pool.get('ir.sequence')
		Configuration = pool.get('formatos_de_ejecucion.configuraciones')
		secuencia_formatos = Configuration.read('1',['secuencia_formatos_de_ejecucion'])[0]['secuencia_formatos_de_ejecucion']
		if secuencia_formatos:
			return Sequence.get_id(int(secuencia_formatos))

	@classmethod
	def create(cls, vlist):
		vlist = [x.copy() for x in vlist]
		for values in vlist:
			if not values.get('codigo'):
				values['codigo'] = cls._new_codigo()
		return super(FormatoDeEjecucion, cls).create(vlist)
	
	@classmethod
	@ModelView.button
	@Workflow.transition('finalizado')
	def finalizar(cls, ids):
		pass
	
	@classmethod
	@ModelView.button
	@Workflow.transition('aprobado')
	def aprobar(cls, ids):
		pass
	
	@classmethod
	@ModelView.button
	@Workflow.transition('finalizado')
	def desaprobar(cls, ids):
		pass
	
	@classmethod
	@ModelView.button
	@Workflow.transition('borrador')
	def aborrador(cls, ids):
		pass

class Ingreso(ModelSQL,ModelView):
	"Ingresos"
	__name__ = 'formatos_de_ejecucion.ingresos'
	_rec_name = 'concepto'
	_states = {'readonly': Eval('formato_de_ejecucion_state') != 'borrador',}
	_depends = ['formato_de_ejecucion_state']

		
	formato_de_ejecucion = fields.Many2One('formatos_de_ejecucion.formatos_de_ejecucion',
										"Formato de Ejecucion",
										required=True,
										states=_states,
										depends=_depends,
										)
	formato_de_ejecucion_state = fields.Function(fields.Selection(STATES, 'Estado del Formato'),'on_change_with_formato_state')
	valor = fields.Integer("Valor",
						required=True,
						states=_states,
						depends=_depends,
						)
	concepto = fields.Char("Concepto",
						required=True,
						states=_states,
						depends=_depends,
						)
	ejecutado = fields.Boolean("Ejecutado")
	
	@fields.depends('formato_de_ejecucion')
	def on_change_with_formato_state(self, name=None):
		if self.formato_de_ejecucion:
			return self.formato_de_ejecucion.state
		return 'borrador'
	
	@classmethod
	def default_formato_de_ejecucion_state(cls):
		return 'borrador'
	
class Gasto(ModelSQL,ModelView):
	"Gastos"
	__name__ = 'formatos_de_ejecucion.gastos'
	_rec_name = 'concepto'
	_states = {'readonly': Eval('formato_de_ejecucion_state') != 'borrador',}
	_depends = ['formato_de_ejecucion_state']
	
	formato_de_ejecucion = fields.Many2One('formatos_de_ejecucion.formatos_de_ejecucion',
										"Formato de Ejecucion",
										required=True,
										states=_states,
										depends=_depends,
										)
	formato_de_ejecucion_state = fields.Function(fields.Selection(STATES, 'Estado del Formato'),'on_change_with_formato_state')
	valor = fields.Integer("Valor",
						required=True,
						states=_states,
						depends=_depends,
						)
	concepto = fields.Char("Concepto",
						required=True,
						states=_states,
						depends=_depends,
						)
	proveedor = fields.Many2One("party.party",
							"Proveedor",
							required=True,
							domain=[('active','=',True),('supplier','=',True)],
							states=_states,
							depends=_depends,
							)
	ejecutado = fields.Boolean("Ejecutado")
	categoria = fields.Many2One('formatos_de_ejecucion.categoria_gastos',
							"Categoria",
							required=True,
							domain=[('activo','=',True)],
							states=_states,
							depends=_depends,
							)

	@fields.depends('formato_de_ejecucion')
	def on_change_with_formato_state(self, name=None):
		if self.formato_de_ejecucion:
			return self.formato_de_ejecucion.state
		return 'borrador'
	
	@classmethod
	def default_formato_de_ejecucion_state(cls):
		return 'borrador'
class GastoReport(Report):
	__name__ = 'formatos_de_ejecucion.gastos'
	
	@classmethod
	def get_context(cls, records, data):
		context = super(GastoReport, cls).get_context(records, data)
		gastos_por_tercero = {} #diccionario de la forma {tercero:[[lista de datos adicionales],[gastos del tercero]}
		for record in records:
			if not(record.proveedor in gastos_por_tercero):
				gastos_por_tercero[record.proveedor]=[
					{'phone':None,'mobile':None,'rut':None,
					'direccion':None,'ciudad':None,'email':None,
					'valor_total':0},
					[]]
				for dato in record.proveedor.contact_mechanisms:
					if (dato.type=='phone'):
						gastos_por_tercero[record.proveedor][0]['phone']=dato.value
					elif (dato.type=='mobile'):
						gastos_por_tercero[record.proveedor][0]['mobile']=dato.value
					elif (dato.type=='email'):
						gastos_por_tercero[record.proveedor][0]['email']=dato.value
				for dato in record.proveedor.identifiers:
					if (dato.type == 'co_vat_31'):
						gastos_por_tercero[record.proveedor][0]['rut']=dato.code
			gastos_por_tercero[record.proveedor][1].append(record)
			gastos_por_tercero[record.proveedor][0]['valor_total'] += int(record.valor)
		context['gastos_por_tercero']=gastos_por_tercero
		return context

	
class CategoriaGasto(ModelSQL,ModelView):
	"Categoria de Gastos"
	__name__ = 'formatos_de_ejecucion.categoria_gastos'
	_rec_name = 'nombre'
	
	nombre = fields.Char("Nombre",required=True)
	activo = fields.Boolean("Activa")
	
	@staticmethod
	def default_activo():
		return True

class Configuracion (ModelSingleton,ModelSQL,ModelView):
	"Confuguraciones"
	__name__='formatos_de_ejecucion.configuraciones'
	_rec_name = 'id'
	secuencia_formatos_de_ejecucion=fields.Many2One('ir.sequence', 'Secuencia para formatos',domain=[('code', '=', 'formatos_de_ejecucion.formatos_de_ejecucion'),],help="Usado para generar el codigo de los formatos de ejecucion.")
	secuencia_programas=fields.Many2One('ir.sequence', 'Secuencia para programas',domain=[('code', '=', 'formatos_de_ejecucion.programas'),],help="Usado para generar el codigo de los Programas.")
	secuencia_proyectos=fields.Many2One('ir.sequence', 'Secuencia para proyectos',domain=[('code', '=', 'formatos_de_ejecucion.proyectos'),],help="Usado para generar el codigo de los Proyectos.")