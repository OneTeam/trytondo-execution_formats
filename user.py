from trytond.model import ModelView, ModelSQL,fields
from trytond.pool import Pool, PoolMeta

__all__=['User']


class User(ModelSQL, ModelView):
	"User"
	__metaclass__ = PoolMeta
	__name__ = 'res.user'
	
	party = fields.Many2One("party.party","Party",help='Tercero vinculado a este usuario')
