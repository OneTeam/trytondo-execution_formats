from trytond.pool import Pool
from .models import *
from .party import *
from .user import *
def register():
	#Registramos los modelos al framework
	Pool.register(
		Programa,
		Proyecto,
		FormatoDeEjecucion,
		Ingreso,
		Gasto,
		CategoriaGasto,
		Configuracion,
		Address,
		Party,
		PartyIdentifier,
		User,
		module="formatos_de_ejecucion", type_="model" )
	
	Pool.register(GastoReport, module="formatos_de_ejecucion", type_="report")
