from trytond.model import ModelView, ModelSQL,fields
from trytond.pool import Pool, PoolMeta
import stdnum.co

__all__=['Address', 'Party','PartyIdentifier']

class Address(ModelSQL, ModelView):
	__metaclass__ = PoolMeta
	__name__ = 'party.address'

	@classmethod
	def search_rec_name(cls, name, clause):
		criterios = super(Address, cls).search_rec_name(name,clause)
		criterios.append(('name',) + tuple(clause[1:]))
		return criterios

class Party(ModelSQL, ModelView):
	__metaclass__ = PoolMeta
	__name__ = 'party.party'
	
	coordinator = fields.Boolean("Coordinador",help='Es coordinador?')
	supplier = fields.Boolean("Supplier",help='Is Supplier?')
	customer = fields.Boolean("Customer",help='Is Customer?',)

class PartyIdentifier(ModelSQL,ModelView):
	"Party Identifer Colombia"
	__name__ = "party.identifier"
	
	@classmethod
	def __setup__(cls):
		super(PartyIdentifier, cls).__setup__()
		cls.type.selection.extend([('co_vat_31','NIT')])
	
	@fields.depends('type','party','code')
	def  check_code(self):
		super(PartyIdentifier,self).check_code()
		if self.type == 'co_vat_31':
			if not stdnum.co.nit.is_valid(self.code):
				self.raise_user_error('invalid_vat', {'code':self.code,'party':self.party.rec_name,})